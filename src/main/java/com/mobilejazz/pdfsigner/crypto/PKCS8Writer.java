package com.mobilejazz.pdfsigner.crypto;

import com.mobilejazz.pdfsigner.usecases.exception.InvalidCertificateException;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;

public class PKCS8Writer {
    private final PrivateKey privateKey;

    public PKCS8Writer(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public void writeToFile(File file, char[] password) throws IOException {
        byte[] encryptedKey;
        try {
            encryptedKey = this.encryptedPrivateKey(password);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(encryptedKey);
        fos.close();
    }


    private byte[] encryptedPrivateKey(char[] password) throws IOException {
        // https://stackoverflow.com/questions/5127379/how-to-generate-a-rsa-keypair-with-a-privatekey-encrypted-with-password

        try {
            // We must use a PasswordBasedEncryption algorithm in order to encrypt the private key, you may use any common algorithm supported by openssl, you can check them in the openssl documentation http://www.openssl.org/docs/apps/pkcs8.html
            final String MYPBEALG = "PBEWithSHA1AndDESede";

            int count = 20;// hash iteration count
            SecureRandom random = new SecureRandom();
            byte[] salt = new byte[8];
            random.nextBytes(salt);

            // Create PBE parameter set
            PBEParameterSpec pbeParamSpec = new PBEParameterSpec(salt, count);
            PBEKeySpec pbeKeySpec = new PBEKeySpec(password);
            SecretKeyFactory keyFac = null;
            try {
                keyFac = SecretKeyFactory.getInstance(MYPBEALG);
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
            SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

            Cipher pbeCipher = Cipher.getInstance(MYPBEALG);

            // Initialize PBE Cipher with key and parameters
            pbeCipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec);

            // Encrypt the encoded Private Key with the PBE key
            byte[] ciphertext = pbeCipher.doFinal(this.privateKey.getEncoded());

            // Now construct  PKCS #8 EncryptedPrivateKeyInfo object
            AlgorithmParameters algparms = AlgorithmParameters.getInstance(MYPBEALG);
            algparms.init(pbeParamSpec);
            EncryptedPrivateKeyInfo encinfo = new EncryptedPrivateKeyInfo(algparms, ciphertext);
            // and here we have it! a DER encoded PKCS#8 encrypted key!
            byte[] encryptedPkcs8 = encinfo.getEncoded();
            return encryptedPkcs8;
        } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException | NoSuchPaddingException | BadPaddingException | InvalidParameterSpecException | InvalidKeySpecException | IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        }
    }
}
