package com.mobilejazz.pdfsigner.crypto;

import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;

import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

public class CertificateProperties {
    private final Certificate certificate;

    public CertificateProperties(Certificate certificate) {
        this.certificate = certificate;
    }

    public String getSubjectCN() {
        X500Name x500name = null;
        try {
            x500name = new JcaX509CertificateHolder((X509Certificate) this.certificate).getSubject();
        } catch (CertificateEncodingException e) {
            throw new RuntimeException(e);
        }
        RDN cn = x500name.getRDNs(BCStyle.CN)[0];

        return IETFUtils.valueToString(cn.getFirst().getValue());
    }

}
