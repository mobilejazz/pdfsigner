package com.mobilejazz.pdfsigner.crypto;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Enumeration;

public class PKCS12Reader {
    private final Certificate certificate;
    private final PrivateKey privateKey;

    public PKCS12Reader(File certificateFile, char[] password) throws IOException, UnrecoverableKeyException {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        FileInputStream fis = new FileInputStream(certificateFile);
        try {
            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
            keystore.load(fis, password);

            // grabs the first alias from the keystore and get the private key
            Enumeration<String> aliases = keystore.aliases();
            String alias = aliases.nextElement();
            this.privateKey = ((PrivateKey) keystore.getKey(alias, password));
            Certificate[] certChain = keystore.getCertificateChain(alias);
            this.certificate = certChain[0];
        } catch (CertificateException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (KeyStoreException e) {
            throw new RuntimeException(e);
        } finally {
            fis.close();
        }
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }
}
