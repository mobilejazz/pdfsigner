package com.mobilejazz.pdfsigner.crypto;

import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

public class PKCS8Reader {

    private final PrivateKey privateKey;

    public PKCS8Reader(File file, char[] password) throws IOException, InvalidKeyException {
        byte[] privKeyByteArray = new byte[(int) file.length()];
        FileInputStream fis = new FileInputStream(file);
        fis.read(privKeyByteArray);
        fis.close();

        try {
            EncryptedPrivateKeyInfo pkInfo = new EncryptedPrivateKeyInfo(privKeyByteArray);
            PBEKeySpec keySpec = new PBEKeySpec(password); // password
            SecretKeyFactory pbeKeyFactory = SecretKeyFactory.getInstance(pkInfo.getAlgName());
            PKCS8EncodedKeySpec encodedKeySpec = pkInfo.getKeySpec(pbeKeyFactory.generateSecret(keySpec));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            this.privateKey = keyFactory.generatePrivate(encodedKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }
}
