package com.mobilejazz.pdfsigner.crypto;

import java.util.Formatter;

public class CryptoStringUtils {

    /**
     * Formats the provided byte array in format ab:cd:ef:12:34, with 15 bytes per row
     * @param bytes Bytes to format
     * @return
     */
    public static String opensslByteToHex(final byte[] bytes)
    {
        Formatter formatter = new Formatter();
        for(int i = 0; i < bytes.length; i++)
        {
            formatter.format("%02x", bytes[i]);
            if(i < bytes.length-1)
                formatter.format(":");
            if(i%15 == 14)
                formatter.format("\n");
        }
        String result = formatter.toString();
        formatter.close();
        // remove last ":"
        result = result.replaceAll(":\n?$","");
        return result;
    }

}
