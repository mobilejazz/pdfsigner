package com.mobilejazz.pdfsigner.crypto;

import com.mobilejazz.pdfsigner.usecases.exception.InvalidCertificateException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;

public class X509CertificateWriter {

    private final Certificate certificate;

    public X509CertificateWriter(Certificate certificate) {
        this.certificate = certificate;
    }

    public void writeToFile(File file) throws IOException, InvalidCertificateException {
        // certificate
        {
            FileOutputStream fos = new FileOutputStream(file);
            try {
                fos.write(this.certificate.getEncoded());
            } catch (CertificateEncodingException e) {
                throw new RuntimeException(e);
            }
            fos.close();
        }
    }

}
