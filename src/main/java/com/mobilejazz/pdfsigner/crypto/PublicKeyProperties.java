package com.mobilejazz.pdfsigner.crypto;

import java.math.BigInteger;
import java.security.interfaces.RSAPublicKey;

public class PublicKeyProperties {

    private final RSAPublicKey publicKey;

    public PublicKeyProperties(RSAPublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public BigInteger getModulus() {
        return this.publicKey.getModulus();
    }

    public String getId() {
        return new BigInteger(getModulus().toString(), 16).toString().substring(0, 32);
    }
}
