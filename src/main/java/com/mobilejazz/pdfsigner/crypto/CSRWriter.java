package com.mobilejazz.pdfsigner.crypto;

import org.bouncycastle.jce.PKCS10CertificationRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class CSRWriter {
    private PKCS10CertificationRequest csr;

    public CSRWriter(PKCS10CertificationRequest csr) {
        this.csr = csr;
    }

    public void writeToFile(File file) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(this.csr.getEncoded());
        fos.close();
    }

}
