package com.mobilejazz.pdfsigner.crypto;

import org.bouncycastle.jce.PKCS10CertificationRequest;

import javax.security.auth.x500.X500Principal;
import java.security.*;

public class CSRGenerator {
    private PKCS10CertificationRequest csr;
    private PrivateKey privateKey;

    public CSRGenerator(String name) {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        // make key pair
        KeyPairGenerator keyGen;
        try {
            keyGen = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        keyGen.initialize(2048, new SecureRandom());
        KeyPair keypair = keyGen.generateKeyPair();

        // subject
        X500Principal subjectName = new X500Principal("CN="+name);

        // CSR
        final String signatureAlgorithm = "SHA256withRSA";
        try {
            this.csr = new PKCS10CertificationRequest(signatureAlgorithm, subjectName, keypair.getPublic(), null, keypair.getPrivate());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (NoSuchProviderException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (SignatureException e) {
            throw new RuntimeException(e);
        }
        this.privateKey = keypair.getPrivate();
    }

    public PKCS10CertificationRequest getCsr() {
        return csr;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }
}
