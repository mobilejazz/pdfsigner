package com.mobilejazz.pdfsigner.crypto;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class X509CertificateReader {

    private final X509Certificate certificate;

    public X509CertificateReader(File certificateFile) throws IOException {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        FileInputStream fis = new FileInputStream(certificateFile);
        try {
            CertificateFactory fact = CertificateFactory.getInstance("X.509");
            this.certificate = (X509Certificate) fact.generateCertificate(fis);
        } catch (CertificateException e) {
            throw new RuntimeException(e);
        } finally {
            fis.close();
        }
    }

    public X509Certificate getCertificate() {
        return certificate;
    }
}
