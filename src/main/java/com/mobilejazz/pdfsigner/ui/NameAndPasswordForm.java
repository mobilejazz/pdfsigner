package com.mobilejazz.pdfsigner.ui;

import javax.swing.*;
import java.util.Arrays;

public class NameAndPasswordForm {
    private JTextField name;
    private JPasswordField password;
    private JPasswordField password2;
    private JPanel panel;

    public boolean show() {
        String name;
        boolean validName = false;
        do {
            String[] options = new String[]{"OK", "Cancel"};
            int option = JOptionPane.showOptionDialog(null, panel, "Certificate creation",
                    JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                    null, options, options[0]);
            if(option != 0)
                return false;

            if(this.getName().length() == 0) {
                JOptionPane.showMessageDialog(this.panel, "Please fill in name", "Certificate creation", JOptionPane.ERROR_MESSAGE);
                continue;
            }
            if(this.getPassword().length < 8) {
                JOptionPane.showMessageDialog(this.panel, "Password should have at least 8 characters", "Certificate creation", JOptionPane.ERROR_MESSAGE);
                continue;
            }
            if(!Arrays.equals(this.getPassword(), password2.getPassword())) {
                JOptionPane.showMessageDialog(this.panel, "Passwords do not match", "Certificate creation", JOptionPane.ERROR_MESSAGE);
                continue;
            }
            validName = true;
        } while(!validName);

        return true;
    }

    public String getName() {
        return this.name.getText();
    }

    public char[] getPassword() {
        return this.password.getPassword();
    }
}
