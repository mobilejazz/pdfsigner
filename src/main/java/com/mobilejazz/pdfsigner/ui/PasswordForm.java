package com.mobilejazz.pdfsigner.ui;

import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

public class PasswordForm {
    final JPanel panel;
    final JPasswordField pass;

    public PasswordForm() {
        panel = new JPanel();
        JLabel label = new JLabel("Enter password:");
        pass = new JPasswordField(32);
        // hack to set cursor when opening form
        pass.addAncestorListener(new AncestorListener() {
            @Override
            public void ancestorAdded(AncestorEvent ae) {
                pass.requestFocus();
            }
            public void ancestorRemoved(AncestorEvent event) {}
            public void ancestorMoved(AncestorEvent event) {}
        });
        panel.add(label);
        panel.add(pass);
    }

    public boolean show() {
        String[] options = new String[]{"OK", "Cancel"};
        int option = JOptionPane.showOptionDialog(null, panel, "Private key",
                JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, options, options[0]);
        return option == 0;
    }

    public char[] getPassword() {
        return this.pass.getPassword();
    }
}
