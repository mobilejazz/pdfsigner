package com.mobilejazz.pdfsigner.ui;

import com.mobilejazz.pdfsigner.model.Identity;
import com.mobilejazz.pdfsigner.preferences.Preferences;
import com.mobilejazz.pdfsigner.usecases.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.List;

public class MainUI {
    private JTextField fileNameTextfield;
    private JButton chooseFileButton;
    private JComboBox identityComboBox;
    private JPanel panel;
    private JButton signButton;

    private final Preferences preferences = new Preferences();
    private List<Identity> identities = null;
    private File fileToSign = null;

    private final String NO_IDENTITIES = "<No identities found>";
    private final String REQUEST_CERTIFICATE = "Request certificate...";
    private final String IMPORT_CERTIFICATE = "Import certificate...";

    public MainUI() {
        reloadIdentities();

        chooseFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                choosePdf();
            }
        });
        identityComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object selectedItem = identityComboBox.getSelectedItem();
                if(selectedItem == REQUEST_CERTIFICATE) {
                    requestCertificate();
                } else if(selectedItem == IMPORT_CERTIFICATE) {
                    importCertificate();
                    reloadIdentities();
                }
            }
        });
        signButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                signPdf();
            }
        });
        fileNameTextfield.setDropTarget(new DropTarget() {
            public synchronized void drop(DropTargetDropEvent evt) {
                try {
                    evt.acceptDrop(DnDConstants.ACTION_COPY);
                    Object transferable = evt.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                    if(! (transferable instanceof List))
                        return;
                    List<File> droppedFiles = (List<File>) transferable;
                    if(droppedFiles.size() > 0)
                        setFileToSign(droppedFiles.get(0));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public void show() {
        JFrame frame = new JFrame("Mobile Jazz PDF Signer");
        try {
            frame.setContentPane(new MainUI().panel);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
        } catch (Exception e) {
            showException(e);
        }
    }

    private void reloadIdentities() {
        try {
            ListCertificates uc = new ListCertificates(preferences.getCertificatesDir());
            identities = uc.getList();
        } catch (IOException e) {
            Exception e1 = new Exception("Could not load user identities", e);
            showException(e1);
            return;
        }
        DefaultComboBoxModel comboBoxOptions = new DefaultComboBoxModel();
        if(identities.size() == 0) {
            comboBoxOptions.addElement(NO_IDENTITIES);
        } else {
            for(Identity i: identities) {
                comboBoxOptions.addElement(i);
            }
        }

        // two fixed options
        comboBoxOptions.addElement(REQUEST_CERTIFICATE);
        comboBoxOptions.addElement(IMPORT_CERTIFICATE);

        identityComboBox.setModel(comboBoxOptions);

        refreshConstraints();
    }

    private void refreshConstraints() {
        String filename = "";
        if(fileToSign != null)
            filename = fileToSign.getName();
        this.fileNameTextfield.setText(filename);
        this.signButton.setEnabled(identities.size() > 0 && fileToSign != null);
    }

    private void choosePdf() {
        // choose file
        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("PDF files", "pdf");
        fc.setFileFilter(filter);
        if(fc.showOpenDialog(panel) != JFileChooser.APPROVE_OPTION)
            return; // cancel or error
        setFileToSign(fc.getSelectedFile());
    }

    private void setFileToSign(File file) {
        this.fileToSign = file;
        refreshConstraints();
    }

    private void signPdf() {
        if( !(identityComboBox.getSelectedItem() instanceof Identity))
            return; // no identity selected
        Identity id = (Identity) identityComboBox.getSelectedItem();

        // enter password
        PasswordForm pf = new PasswordForm();
        if(!pf.show())
            return;

        try {
            SignPdf uc = new SignPdf(preferences.getCertificatesDir(), id, pf.getPassword());
            uc.sign(fileToSign);
            JOptionPane.showMessageDialog(panel, "Signature succeeded", "Sign PDF", JOptionPane.INFORMATION_MESSAGE);
        } catch (InvalidKeyException e) {
            showException(new Exception("Invalid password", e));
        } catch (Exception e) {
            showException(e);
        }
    }

    private void requestCertificate() {
        NameAndPasswordForm form = new NameAndPasswordForm();
        if(!form.show())
            return; //cancelled

        CertificateRequest csr = new CertificateRequest(form.getName(), form.getPassword());

        // file dialog
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("CSR files", "csr");
        fileChooser.setFileFilter(filter);
        if (fileChooser.showSaveDialog(this.panel) != JFileChooser.APPROVE_OPTION)
            return;

        // save to file
        File file = fileChooser.getSelectedFile();
        // force .csr extension
        if(!file.toString().contains(".")) file = new File(file.toString() + ".csr");
        try {
            csr.writeKeyToPath(preferences.getCertificatesDir());
            csr.writeCsrToFile(file);
        } catch (IOException e) {
            showException(e);
            return;
        }

        // display form to print
        String requestText = csr.getWrittenDocumentRequest();
        CertificateRequestPrintingDialog d = new CertificateRequestPrintingDialog(requestText);
        d.pack();
        d.setVisible(true);
    }

    private void importCertificate() {
        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Certificate files", "crt", "p12");
        fc.setFileFilter(filter);
        if(fc.showOpenDialog(panel) != JFileChooser.APPROVE_OPTION)
            return; // cancel or error

        File certificate = fc.getSelectedFile();
        try {
            if(certificate.getName().endsWith(".p12")) {
                // enter password
                PasswordForm pf = new PasswordForm();
                if(!pf.show())
                    return;

                ImportPKCS12 i = new ImportPKCS12(certificate, pf.getPassword());
                i.writeToPath(preferences.getCertificatesDir());
            } else {
                ImportCertificate i = new ImportCertificate(certificate);
                i.writeToPath(preferences.getCertificatesDir());
            }
        } catch (Exception e) {
            showException(e);
        }
    }

    private void showException(Exception e) {
        e.printStackTrace();
        JOptionPane.showMessageDialog(panel, e.getLocalizedMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
}
