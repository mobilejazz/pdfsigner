package com.mobilejazz.pdfsigner.ui;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class CertificateRequestPrintingDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextArea textArea;
    private JButton saveToFileButton;

    public CertificateRequestPrintingDialog(String requestText) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        this.textArea.setText(requestText);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());
        saveToFileButton.addActionListener(e -> onSaveToFile());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        try {
            if(!this.textArea.print())
                return; // cancelled
            int answer = JOptionPane.showOptionDialog(this.contentPane, "Did you sign the document?", "Printing confirmation",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                    new String[] {"YES", "NO"}, JOptionPane.NO_OPTION);
            if(answer == JOptionPane.YES_OPTION)
                dispose();
        } catch (PrinterException e) {
            showException(e);
        }
    }

    private void onSaveToFile() {
        // file dialog
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT files", "txt");
        fileChooser.setFileFilter(filter);
        if (fileChooser.showSaveDialog(this.contentPane) != JFileChooser.APPROVE_OPTION)
            return;

        // save to file
        File file = fileChooser.getSelectedFile();
        // force .txt extension
        if(!file.toString().contains(".")) file = new File(file.toString() + ".txt");
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(this.textArea.getText().getBytes());
            fos.close();
        } catch (IOException e) {
            showException(e);
            return;
        }

        // close
        int answer = JOptionPane.showOptionDialog(this.contentPane, "Did you print and sign the document?", "Printing confirmation",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                new String[] {"YES", "NO"}, JOptionPane.NO_OPTION);
        if(answer == JOptionPane.YES_OPTION)
            dispose();
    }

    private void onCancel() {
        int answer = JOptionPane.showOptionDialog(this.contentPane,
                "If you do not print and sign this document, the request will be cancelled and you will have to restart the process. Are you sure you want to cancel?",
                "Document not signed",
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null,
                new String[] {"YES", "NO"}, JOptionPane.NO_OPTION);
        if(answer == JOptionPane.YES_OPTION)
            dispose();
    }

    private void showException(Exception e) {
        e.printStackTrace();
        JOptionPane.showMessageDialog(this.contentPane, e.getLocalizedMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static void main(String[] args) {
        CertificateRequestPrintingDialog dialog = new CertificateRequestPrintingDialog("Test");
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
