package com.mobilejazz.pdfsigner.preferences;

import java.io.File;
import java.io.IOException;

public class Preferences {

    public File getCertificatesDir() throws IOException {
        String folderName = System.getProperty("user.home");
        File result = new File(folderName,".mjpdfsigner");
        result.mkdir();
        if(!result.exists())
            throw new IOException("Can not create preferences directory");
        return result;
    }
}
