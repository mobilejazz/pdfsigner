package com.mobilejazz.pdfsigner;

import com.mobilejazz.pdfsigner.ui.MainUI;

import javax.swing.*;

public class MJPDFSigner {
    public static void main(String args[]) {
        System.setProperty("apple.laf.useScreenMenuBar", "true");
        System.setProperty("com.apple.mrj.application.apple.menu.about.name", "MJ PDF Signer");

        // set the look and feel
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new MainUI().show();
            }
        });
    }
}
