package com.mobilejazz.pdfsigner.usecases.exception;

public class InvalidCertificateException extends Exception {

    public InvalidCertificateException() {
        super("Invalid certificate file, no corresponding private key found");
    }
}
