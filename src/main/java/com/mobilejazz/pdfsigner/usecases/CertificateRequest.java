package com.mobilejazz.pdfsigner.usecases;

import com.mobilejazz.pdfsigner.crypto.*;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.RSAPublicKey;

public class CertificateRequest {

    final private CSRGenerator csrGenerator;
    final private String name;
    final private char[] password;

    public CertificateRequest(String name, char[] password) {
        this.name = name;
        this.password = password;

        this.csrGenerator = new CSRGenerator(name);
    }

    public void writeCsrToFile(File file) throws IOException {
        new CSRWriter(this.csrGenerator.getCsr()).writeToFile(file);
    }

    public void writeKeyToPath(File baseFolder) throws IOException {
        try {
            String baseName = new PublicKeyProperties((RSAPublicKey) this.csrGenerator.getCsr().getPublicKey()).getId();

            new CSRWriter(this.csrGenerator.getCsr()).writeToFile(new File(baseFolder, baseName + ".csr"));
            new PKCS8Writer(this.csrGenerator.getPrivateKey()).writeToFile(new File(baseFolder, baseName + ".key"), this.password);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    public String getWrittenDocumentRequest() {
        try {
            BigInteger modulus = new PublicKeyProperties((RSAPublicKey) this.csrGenerator.getCsr().getPublicKey()).getModulus();
            String modulusString = CryptoStringUtils.opensslByteToHex(modulus.toByteArray());

            return  "Digital signature certificate request\n" +
                    "-------------------------------------\n" +
                    "\n" +
                    "I, "+this.name+", declare that I own a private key with the modulus:\n" +
                    "\n" +
                    modulusString + "\n" +
                    "\n" +
                    "To my knowledge, nobody else knows the private key. \n" +
                    "I will notify if I suspect it is compromised.\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "Signed: "+ this.name;
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String args[]) throws IOException {
        char[] password = "1234".toCharArray();
        CertificateRequest csr = new CertificateRequest("Jordi", password);
        csr.writeCsrToFile(new File("/tmp/test.csr"));
        csr.writeKeyToPath(new File("/tmp"));
    }

}
