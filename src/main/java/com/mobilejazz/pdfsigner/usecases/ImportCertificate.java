package com.mobilejazz.pdfsigner.usecases;

import com.mobilejazz.pdfsigner.crypto.PublicKeyProperties;
import com.mobilejazz.pdfsigner.crypto.X509CertificateReader;
import com.mobilejazz.pdfsigner.crypto.X509CertificateWriter;
import com.mobilejazz.pdfsigner.usecases.exception.InvalidCertificateException;

import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;

public class ImportCertificate {

    private final X509Certificate cer;

    public ImportCertificate(File certificateFile) throws IOException {
        X509CertificateReader certificateReader = new X509CertificateReader(certificateFile);
        this.cer = certificateReader.getCertificate();
    }

    public void writeToPath(File baseFolder) throws IOException, InvalidCertificateException {
        this.validateKey(baseFolder);

        String baseName = new PublicKeyProperties((RSAPublicKey) this.cer.getPublicKey()).getId();
        X509CertificateWriter certificateWriter = new X509CertificateWriter(this.cer);
        certificateWriter.writeToFile(new File(baseFolder, baseName+".crt"));
    }

    private void validateKey(File baseFolder) throws InvalidCertificateException {
        String baseName = new PublicKeyProperties((RSAPublicKey) this.cer.getPublicKey()).getId();
        File keyFile = new File(baseFolder, baseName+".key");
        if(!keyFile.exists()) {
            throw new InvalidCertificateException();
        }
    }

    public static void main(String args[]) throws IOException {
        ImportCertificate csr = new ImportCertificate(new File("/Users/gimix/git/adan/easy-ca/test/test1/test.crt"));
        try {
            csr.writeToPath(new File("/tmp"));
        } catch (InvalidCertificateException e) {
            e.printStackTrace();
        }
    }

}
