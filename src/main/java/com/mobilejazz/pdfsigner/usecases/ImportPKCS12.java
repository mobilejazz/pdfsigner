package com.mobilejazz.pdfsigner.usecases;

import com.mobilejazz.pdfsigner.crypto.PKCS12Reader;
import com.mobilejazz.pdfsigner.crypto.PKCS8Writer;
import com.mobilejazz.pdfsigner.crypto.PublicKeyProperties;
import com.mobilejazz.pdfsigner.crypto.X509CertificateWriter;
import com.mobilejazz.pdfsigner.usecases.exception.InvalidCertificateException;

import java.io.File;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.interfaces.RSAPublicKey;

public class ImportPKCS12 {

    private final Certificate certificate;
    private final PrivateKey privateKey;
    private final char[] password;

    public ImportPKCS12(File certificateFile, char[] password) throws IOException, UnrecoverableKeyException {
        this.password = password;

        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        PKCS12Reader pkcs12Reader = new PKCS12Reader(certificateFile, password);
        this.certificate = pkcs12Reader.getCertificate();
        this.privateKey = pkcs12Reader.getPrivateKey();
    }

    public void writeToPath(File baseFolder) throws IOException, InvalidCertificateException {
        String baseName = new PublicKeyProperties((RSAPublicKey) this.certificate.getPublicKey()).getId();

        X509CertificateWriter x509CertificateWriter = new X509CertificateWriter((this.certificate));
        x509CertificateWriter.writeToFile(new File(baseFolder, baseName + ".crt"));

        PKCS8Writer pkcs8writer = new PKCS8Writer(this.privateKey);
        pkcs8writer.writeToFile(new File(baseFolder, baseName+".key"), this.password);
    }

}
