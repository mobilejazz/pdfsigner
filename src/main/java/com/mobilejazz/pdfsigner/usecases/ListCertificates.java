package com.mobilejazz.pdfsigner.usecases;

import com.mobilejazz.pdfsigner.model.Identity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListCertificates {

    private final File baseDir;

    public ListCertificates(File baseDir) {
        this.baseDir = baseDir;
    }

    public List<Identity> getList() throws IOException {
        ArrayList<Identity> result = new ArrayList<>();
        for(File file: baseDir.listFiles()){
            if(file.isFile()){
                if(file.getName().endsWith(".crt")){
                    LoadCertificateIdentity loader = new LoadCertificateIdentity(file);
                    Identity i = loader.getIdentity();
                    result.add(i);
                }
            }
        }
        return result;
    }
}
