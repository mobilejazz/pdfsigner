package com.mobilejazz.pdfsigner.usecases;

import com.mobilejazz.pdfsigner.crypto.PKCS8Reader;
import com.mobilejazz.pdfsigner.crypto.X509CertificateReader;
import com.mobilejazz.pdfsigner.model.Identity;
import com.mobilejazz.pdfsigner.usecases.signing.CreateSignature;
import com.mobilejazz.pdfsigner.usecases.signing.TSAClient;

import java.io.File;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;

public class SignPdf {

    private final Certificate certificate;
    private final PrivateKey privateKey;

    public SignPdf(File baseDir, Identity identity, char[] password) throws IOException, InvalidKeyException {
        File certificateFile = new File(baseDir, identity.getId() + ".crt");
        File keyFile = new File(baseDir, identity.getId() + ".key");

        this.certificate = new X509CertificateReader(certificateFile).getCertificate();
        this.privateKey = new PKCS8Reader(keyFile, password).getPrivateKey();
    }

    public void sign(File pdf) throws IOException {
        TSAClient tsaClient = null; // no TSA
        boolean externalSig = false; // signing locally

        // sign PDF
        CreateSignature signing = null;
        try {
            signing = new CreateSignature(new Certificate[]{certificate}, this.privateKey);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        signing.setExternalSigning(externalSig);

        String name = pdf.getName();
        String substring = name.substring(0, name.lastIndexOf('.'));

        File outFile = new File(pdf.getParent(), substring + "_signed.pdf");
        signing.signDetached(pdf, outFile, tsaClient);
    }
}
