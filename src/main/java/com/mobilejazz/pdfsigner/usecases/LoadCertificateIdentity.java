package com.mobilejazz.pdfsigner.usecases;

import com.mobilejazz.pdfsigner.crypto.CertificateProperties;
import com.mobilejazz.pdfsigner.crypto.PublicKeyProperties;
import com.mobilejazz.pdfsigner.crypto.X509CertificateReader;
import com.mobilejazz.pdfsigner.model.Identity;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.Security;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;

public class LoadCertificateIdentity {
    private final X509Certificate cer;

    public LoadCertificateIdentity(File certificateFile) throws IOException {
        this.cer = new X509CertificateReader(certificateFile).getCertificate();
    }

    public Identity getIdentity() {
        String cn = new CertificateProperties(this.cer).getSubjectCN();
        String id = new PublicKeyProperties((RSAPublicKey) this.cer.getPublicKey()).getId();

        return new Identity(cn, id);
    }

}
