package com.mobilejazz.pdfsigner.model;

public class Identity {
    private final String name;
    private final String id;

    public Identity(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String toString() {
        return this.getName() + " (" + this.getId() + ")";
    }
}
