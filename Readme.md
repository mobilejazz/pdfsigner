# Mobile Jazz PDF Signer

The PDF Signer is a tool designed to digitally sign internal documents in the company.

It provides the following functionality:

 * Creation of digital certificate requests (CSR)
 * Importing digital certificates
 * Signing PDF documents

The following requirements have been taken into account:

 * The solution should be easy to use for non tech-savvy users
 * Should provide a level of security equivalent to other tools such as Adobe Pro
 * Should work on Windows, Linux and macOS

## How to use

### Installing
 1. Download the tool from this repository
 1. Run the tool by double-clicking on the downloaded .jar file. You will need Java version 7 or better.

### Requesting a certificate from a Certificate Authority
If you do not have a digital certificate yet, you can request one using the tool. Please note the tool itself *does not* generate certificates, you need a CA to generate one for you. It can be an official, recognized CA or an internal CA (see the References section at the end for more information).

 1. Request a certificate by opening the **Identity** drop-down and selecting **Request certificate...**
 1. Enter your full name and a password (you will use that password for signing). The password needs to be at least 8 characters long. You need to remember this password, it can not be recovered.
 1. Save the resulting CSR file in a place you can find later (for example the Desktop)
 1. A certificate request form will appear. At Mobile Jazz we use this form to record the certificate request, we print this form and sign it, then hand it in to the responsible of the CA for certificate creation.
 1. As a response from the CA, you will receive a .crt file, put it somewhere you recall, for example your Desktop. Then import this file by opening the **Identity** drop-down and selecting **Import certificate...**
 1. You're done, now you can sign files

### Importing a digital certificate with private key
If you already have a digital certificate in form of a PKCS12 file (usually `.p12` extension), you can also import it for use in this tool.

 1. Then import this file by opening the **Identity** drop-down and selecting **Import certificate...**
 1. You're done, now you can sign files

### Signing a PDF file
 1. Select the identity you want to use from the **Identity** dropdown. Usually you will only have one identity, but you could have more.
 1. Use the **Choose...** button to find the file you want to sign
 1. Press **Sign** and provide your password
 1. If everything went all right, you can now find a file next to the original PDF file, with the `_signed` suffix.

## How to build
```
./gradlew build
```

The resulting JAR will be in `build/libs`.

# References
 * [easy-ca](https://github.com/redredgroovy/easy-ca): a project to help in the creation and administration of a Certificate Authority for internal use.
